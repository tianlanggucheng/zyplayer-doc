package com.zyplayer.doc.db.framework.db.dto;

import lombok.Data;

/**
 * 数据库信息
 *
 * @author 暮光：城中城
 * @since 2018-11-27
 */
@Data
public class DatabaseInfoDto {
	private String dbName;
}
